# Moni Challenge

Clonamos el proyecto
```bash
git clone https://gitlab.com/Olivaa/moni_challenge.git
```

Nos movemos al directorio del proyecto
```bash
cd moni_challenge
```

Levantamos el proyecto
```bash
docker-compose up
```
Una vez ejecutado va a clonar las imagenes que vamos a utilizar en el proyecto


Apretamos CTRL+C y volvemos a levantar el proyecto 
(debido a que la db demora en iniciarse y Django no puede conectarse)
```bash
docker-compose up
```

Corremos las migraciones con el proyecto levantado
```bash
docker-compose run backend python manage.py migrate
```

Creamos un admin user
```bash
docker-compose run backend python manage.py createsuperuser
```

En cualquier navegador nos dirigimos a **localhost:3000**

Para iniciar sesion utilizamos el email y la contraseña del superuser creado anteriormente


# Tecnologias utilizadas
**Backend**:
    Django

**Frontend**:
    React Js

**Database**:
    MySQL

Docker y docker-compose
