# Actualizar imagenes

Luego de realizar los cambios necesarios de la imagen, esta se debe actualizar mediante los siguientes pasos:

- Entrar al directorio de la imagen
- Actualizar el archivo TAG con la nueva version
- Ejecutar el siguiente comando:
```bash
docker build -t `cat TAG` -f ./Dockerfile <path_al_repositorio>
```