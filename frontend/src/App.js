import React, { useState } from 'react';
import LoanApplicationForm from './components/LoanApplicationForm/LoanApplicationForm';
import Login from './components/Administration/Login';
import Dashboard from './components/Administration/Dashboard';

function App() {
  const [login, setLogin] = useState(null)

  return (
    <div className="App">
      { login === null ? (
        <div>
          <LoanApplicationForm/>
          <div className="d-flex justify-content-center">
            <button className="btn btn-primary" onClick={() => { setLogin('Logued') }}>
              Admin Page
            </button>
          </div> 
        </div>
      ) : (
        <div>
          <Login/>
          <div className="d-flex justify-content-center">
              <button className="btn btn-primary" onClick={() => { setLogin(null) }}>
                Volver a Solicitud
              </button>
          </div>
        </div>
      )}
    </div>
  );
}

export default App;
