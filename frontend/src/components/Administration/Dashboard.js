import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { api } from '../../api'

const Dashboard = ( {access_token} ) => {
    const [loans, setLoans] = useState([]);
    const [editingLoan, setEditingLoan] = useState(null);
    const [controler, setControler] = useState(null)

    useEffect(() => {
        const handleGetLoans = async () => {
            try {
                axios.defaults.headers.common['Authorization'] = `Bearer ${access_token}`;
                const res = await axios.get(api.loanApplication);
                setLoans(res.data);
            } catch (err) {
                console.error(err);
            }
        };

        setControler(null)
        handleGetLoans();
    }, [access_token, controler]);

    const handleDelete = async (dni) => {
        try {
            axios.defaults.headers.common['Authorization'] = `Bearer ${access_token}`;
            const res = await axios.delete(api.updateDestroyLoan(dni));
            console.log(res)
            setControler('')
        } catch (err) {
            console.error(err);
        }
    }

    const handleUpdate = async (loan, dni) => {
        const body = {
            'dni': loan.dni,
            'nombre': loan.nombre,
            'apellido': loan.apellido,
            'genero': loan.genero,
            'monto_solicitado': loan.monto_solicitado,
            'email': loan.email,
            'resultado_aprobacion': loan.resultado_aprobacion,
        }

        try {
            setEditingLoan(null)
            axios.defaults.headers.common['Authorization'] = `Bearer ${access_token}`;
            const res = await axios.put(api.updateDestroyLoan(dni), body);
            setControler('')
        } catch (err) {
            console.error(err);
        }
    }

    return (
    <div>
        <link
        rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css"
        integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65"
        crossorigin="anonymous"
        />
            {editingLoan === null ? (
                <div>
                <table className="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>DNI</th>
                            <th>Genero</th>
                            <th>Monto Solicitado</th>
                            <th>Email</th>
                            <th>Estado Solicitud</th>
                            <th>Fecha Solicitud</th>
                            <th>Fecha Actualizacion</th>
                        </tr>
                    </thead>
                    <tbody>
                        {loans.map((loan) => (
                            <tr key={loan.dni}>
                                <td>{loan.nombre}</td>
                                <td>{loan.apellido}</td>
                                <td>{loan.dni}</td>
                                <td>{loan.genero}</td>
                                <td>{loan.monto_solicitado}</td>
                                <td>{loan.email}</td>
                                <td>{loan.resultado_aprobacion ? 'Aprobado' : 'Rechazado'}</td>
                                <td>{loan.fecha_solicitud}</td>
                                <td>{loan.fecha_actualizacion}</td>
                                <td>
                                  <button className="btn btn-primary me-2" onClick={() => setEditingLoan(loan)}>Editar</button>
                                </td>
                                <td>
                                  {/* Se deberia mejorar la implementacion del borrado para que sea como en el caso de la edicion */}
                                  <button className="btn btn-secondary" onClick={() => {
                                    if (window.confirm('¿Está seguro que desea borrar este préstamo?')) {
                                        handleDelete(loan.dni);
                                        }
                                    }}>Borrar</button>
                                </td>
                            </tr>  
                        ))}
                    </tbody>
                </table>
                </div>
            ) : (
                <div>
                    <h1 className="mt-5 text-center">Editar préstamo</h1>
                    <form className="container mt-5">
                    <div className="mb-3">
                        <label htmlFor="nombre" className="form-label">
                            Nombre
                        </label>
                        <input
                            type="text"
                            className="form-control"
                            id="nombre"
                            value={editingLoan.nombre}
                            onChange={(e) =>
                              setEditingLoan({ ...editingLoan, nombre: e.target.value })
                            }
                        />
                        <label htmlFor="apellido" className="form-label">
                            Apellido
                        </label>
                        <input
                            type="text"
                            className="form-control"
                            id="apellido"
                            value={editingLoan.apellido}
                            onChange={(e) =>
                              setEditingLoan({ ...editingLoan, apellido: e.target.value })
                            }
                        />
                        <label htmlFor="dni" className="form-label">
                            DNI
                        </label>
                        <input
                            type="text"
                            className="form-control"
                            id="dni"
                            value={editingLoan.dni}
                            onChange={(e) =>
                              setEditingLoan({ ...editingLoan, dni: e.target.value })
                            }
                        />
                        <label htmlFor="genero" className="form-label">
                            Género
                        </label>
                        <select
                          className="form-control"
                          id="genero"
                          value={editingLoan.genero}
                          onChange={(e) =>
                            setEditingLoan({ ...editingLoan, genero: e.target.value })
                          }
                        >
                          <option value="M">Masculino</option>
                          <option value="F">Femenino</option>
                          <option value="O">Otro</option>
                        </select>

                        <label htmlFor="monto_solicitado" className="form-label">
                            Monto Solicitado
                        </label>
                        <input
                            type="text"
                            className="form-control"
                            id="monto_solicitado"
                            value={editingLoan.monto_solicitado}
                            onChange={(e) =>
                              setEditingLoan({ ...editingLoan, monto_solicitado: e.target.value })
                            }
                        />

                        <label htmlFor="email" className="form-label">
                            Email
                        </label>
                        <input
                            type="text"
                            className="form-control"
                            id="email"
                            value={editingLoan.email}
                            onChange={(e) =>
                              setEditingLoan({ ...editingLoan, email: e.target.value })
                            }
                        />

                        <label htmlFor="resultado_aprobacion" className="form-label">
                            Estado Solicitud
                        </label>
                        <select
                          className="form-control"
                          id="resultado_aprobacion"
                          value={editingLoan.resultado_aprobacion}
                          onChange={(e) =>
                            setEditingLoan({ ...editingLoan, resultado_aprobacion: e.target.value })
                          }
                        >
                          <option value="true">Aprobado</option>
                          <option value="false">Rechazado</option>
                        </select>
                    </div>
                    <button className="btn btn-primary me-2" onClick={() => handleUpdate(editingLoan, editingLoan.dni)}> Confirmar </button>
                    <button className="btn btn-secondary" onClick={() => setEditingLoan(null)}> Cancelar </button>
                    </form> 
                </div>
            )}  
    </div>
    );
}

export default Dashboard;