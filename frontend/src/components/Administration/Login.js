import React, { useState } from 'react';
import axios from 'axios';
import Dashboard from './Dashboard';
import { api } from '../../api'

const Login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [adminPanel, setAdminPanel] = useState(null)
  const [token, setToken] = useState('')

  const handleLogin = async (event) => {
    event.preventDefault();
    const formData = new FormData();
    formData.append('email', email)
    formData.append('password', password)

    try {
      const res = await axios.post(api.loginUrl, formData);
      const token = res.data.access_token
      setToken(token)
      setAdminPanel('Logued')
      
    } catch (err) {
      console.error(err);
    }

  };

  return (
    <div>{
      adminPanel === null ? (
      <div>
        <h2 className="text-center mt-5">Iniciar sesión</h2>
        <div className="container mt-5">
          <link
          rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css"
          integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65"
          crossorigin="anonymous"
          />
          <form onSubmit={handleLogin}>
            <div className="d-flex justify-content-center mb-3">
              <label className="form-label">
              Email
              <input className="form-control" type="text" value={email} onChange={(e) => setEmail(e.target.value)} />
              </label>
            </div>
            <div className="d-flex justify-content-center mb-3">
              <label className="form-label">
              Contraseña
              <input className="form-control" type="password" value={password} onChange={(e) => setPassword(e.target.value)} />
              </label>
            </div>
            <div className="d-flex justify-content-center mb-3">
              <button className="btn btn-primary me-2" type="submit">Iniciar sesión</button>
            </div>
          </form>
        </div>

      </div>
      ) : (
        <Dashboard access_token={token}/>
      )}
    </div>
  );
};

export default Login;
