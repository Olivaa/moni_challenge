import React from "react";

const ApplicationStatus = (app_status) => {
    return(
        <div>
            { app_status.app_status ? (
                <div className="p-3 mb-2 bg-success text-white">
                    <h1 className="text-center"> Tu Solicitud de Prestamo fue APROBADA </h1>
                </div>
            ) : (
                <div className="p-3 mb-2 bg-danger text-white">
                    <h1 className="text-center"> Tu Solicitud de Prestamo fue RECHAZADA </h1>
                </div>
            )}
        </div>
    )
}

export default ApplicationStatus