import React, { useEffect, useState } from "react";
import axios from 'axios';
import { api } from '../../api'

import ApplicationStatus from "./ApplicationStatus";

const LoanApplicationForm = () => {
  const [dni, setDni] = useState('');
  const [nombre, setNombre] = useState('');
  const [apellido, setApellido] = useState('');
  const [genero, setGenero] = useState('');
  const [email, setEmail] = useState('');
  const [montoSolicitado, setMontoSolicitado] = useState('');
  const [statusResult, setStatusResult] = useState(null)
  const [controler, setControler] = useState(null)

  const handleSubmit = async (event) => {
    event.preventDefault();
    
    const applicationData = {
      'dni': dni,
      'nombre': nombre,
      'apellido': apellido,
      'genero': genero,
      'email': email,
      'monto_solicitado': montoSolicitado
    }
    
    try {
      const res = await axios.post(api.loanApplication, applicationData);
      const status_result = res.data.resultado_aprobacion
      setStatusResult(status_result)
    } catch (err) {
      console.error(err);
    }
  };

  useEffect(() => {
    if(statusResult !== null){
      setControler('')
    }
  }, [statusResult])

  return (
    <div>
        <div>
          <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css"
            integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65"
            crossorigin="anonymous"
            />
          <h2 className="text-center mt-5"> Solicitud de Prestamo </h2>
          <div className="p-3 mb-2 bg-info-subtle text-emphasis-info">  
            <form onSubmit={handleSubmit} className="container mt-5">
              <div className="form-group">
                <div className="row">
                  <div className="col">
                    <label htmlFor="nombre">Nombre:</label>
                    <input
                      type="text"
                      id="nombre"
                      value={nombre}
                      onChange={(event) => setNombre(event.target.value)}
                      className="form-control"
                      required
                      />
                  </div>
                  <div className="col">
                    <label htmlFor="apellido">Apellido:</label>
                    <input
                      type="text"
                      id="apellido"
                      value={apellido}
                      onChange={(event) => setApellido(event.target.value)}
                      className="form-control"
                      required
                      />
                  </div>
                </div>
              </div>
              <div className="form-group">
                <div className="row">
                  <div className="col">
                    <label htmlFor="dni">DNI:</label>
                    <input
                      type="text"
                      id="dni"
                      value={dni}
                      onChange={(event) => setDni(event.target.value)}
                      className="form-control"
                      required
                      />
                  </div>
                  <div className="col">
                    <label htmlFor="genero">Genero:</label>
                    <select
                      id="genero"
                      value={genero}
                      onChange={(event) => setGenero(event.target.value)}
                      className="form-control"
                      required
                      >
                      <option value="">Selecciona tu genero</option>
                      <option value="M">Masculino</option>
                      <option value="F">Femenino</option>
                      <option value="O">Otro</option>
                    </select>
                  </div>
                </div>
              </div>
              <div className="form-group">
                <label htmlFor="email">Email:</label>
                <input
                  type="email"
                  id="email"
                  value={email}
                  onChange={(event) => setEmail(event.target.value)}
                  className="form-control"
                  required
                  />
              </div>
              <div className="form-group">
                <label htmlFor="montoSolicitado">Monto Solicitado:</label>
                <input
                  type="number"
                  id="montoSolicitado"
                  value={montoSolicitado}
                  onChange={(event) => setMontoSolicitado(event.target.value)}
                  className="form-control"
                  min="1000"
                  max="310000"
                  required
                  />
              </div>
              { controler === null ? (
                <div className="d-flex justify-content-center">
                  <button className="btn btn-primary mt-5" type="submit">
                    Enviar Solicitud
                  </button>
                </div>
              ): (
                <div className="mt-3">
                  <ApplicationStatus app_status={statusResult}/>
                </div>
              )}
            </form>
          </div>
        </div>
    </div>
  );
};

export default LoanApplicationForm;
