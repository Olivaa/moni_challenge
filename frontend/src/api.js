const BACKEND_URL = 'http://0.0.0.0:8000'

const api = {
    loginUrl: `${BACKEND_URL}/login/`,
    loanApplication: `${BACKEND_URL}/solicitudes/`,
    updateDestroyLoan: (dni) => `${BACKEND_URL}/solicitudes/${dni}`,
};

export { api };