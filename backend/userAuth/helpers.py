import requests
import json

def createAccessToken(email, password):
    url = 'http://0.0.0.0:8000/api/token/'
    body = {
        "email": email,
        "password": password
    }
    response = requests.post(url, json=body)
    response_content = json.loads(response.content.decode('utf-8'))
    access_token = response_content['access']
    
    return access_token