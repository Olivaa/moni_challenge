from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from django.contrib.auth import authenticate, login
from .serializers import UserSerializer
from .helpers import createAccessToken

class UserLoginView(APIView):
    '''
    Vista que maneja la autenticacion de usuarios administradores.
    
    Methods:
        POST
    
    Args:
        Se requiere del email y la password del usuario administrador:\n
        {
            'email': email,
            'password': password
        }
    Response:
        Retorna el token de acceso si el usuario es autenticado con exito
        status code:
        - 200: Usuario autenticado con exito
        - 401: Credenciales incorrectas o usuario desactivado
    '''
    serializer_class = UserSerializer

    def post(self, request, format=None):
        email = request.data.get('email')
        password = request.data.get('password')

        user = authenticate(email=email, password=password)
        if user:
            if user.is_active:
                login(request, user)
                # Create access token
                token = createAccessToken(email, password)
                return Response({
                    'access_token': token
                }, status=status.HTTP_200_OK)
            else:
                return Response({
                    'error': 'Este usuario ha sido desactivado'
                }, status=status.HTTP_401_UNAUTHORIZED)
        else:
            return Response({
                'error': 'Credenciales incorrectas'
            }, status=status.HTTP_401_UNAUTHORIZED)
