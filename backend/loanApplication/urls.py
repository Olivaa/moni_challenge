from django.urls import path
from .views import LoanApplicationList, LoanApplicationDetail


urlpatterns = [
    path('solicitudes/', LoanApplicationList.as_view()),
    path('solicitudes/<str:pk>', LoanApplicationDetail.as_view())
]