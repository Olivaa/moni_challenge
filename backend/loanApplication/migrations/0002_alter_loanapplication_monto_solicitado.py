# Generated by Django 4.2 on 2023-04-10 03:28

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('loanApplication', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='loanapplication',
            name='monto_solicitado',
            field=models.IntegerField(validators=[django.core.validators.MinValueValidator(1000), django.core.validators.MaxValueValidator(310000)]),
        ),
    ]
