from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

class LoanApplication(models.Model):
    ''' Modelo de Solicitud de Prestamo '''
    dni = models.CharField(max_length=8, primary_key=True, null=False)
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    GENERO_CHOICES = [
        ('M', 'Masculino'),
        ('F', 'Femenino'),
        ('O', 'Otro')
    ]
    genero = models.CharField(max_length=1, choices=GENERO_CHOICES)
    email = models.EmailField()
    monto_solicitado = models.IntegerField(
        validators=[
            MinValueValidator(1000),
            MaxValueValidator(310000),
        ]
    )
    resultado_aprobacion = models.BooleanField(default=False)
    fecha_solicitud = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True, null=True)
    
    def __str__(self):
        estado_solicitud = self.aprobacion(self.resultado_aprobacion)
        string = f'''El estado de solicitud del prestamo de {self.nombre} {self.apellido}
                     por un monto de {self.monto_solicitado} pesos fue {estado_solicitud}'''
        return string