from rest_framework import generics, mixins
from .models import LoanApplication
from .serializers import LoanApplicationSerializer
from .helpers import get_aprobacion
from .permissions import IsPostOrIsAuthenticated


class LoanApplicationList(generics.GenericAPIView, mixins.ListModelMixin, mixins.CreateModelMixin):
    '''
    Vista que maneja las solicitudes de prestamo.
    
    Esta vista permite devolver todas las solicitudes de prestamo solo si el usuario está autenticado,
    o crear una nueva solicitud sin la necesidad de autenticarse.
    
    Methods:
        GET, POST
    
    Args:
        GET:\n
        No se requieren argumentos adicionales para este metodo
        
        POST:\n
        Se requieren los siguientes argumentos:\n
        {
            'dni': dni,
            'nombre': nombre,
            'apellido': apellido,
            'genero': genero,
            'email': email,
            'monto_solicitado': monto solicitado
        }
        
    Response:
        GET: \n
        Retorna todas las solicitudes de prestamo para usuarios autenticados
        status code 200
        POST: \n
        Retorna un objeto JSON que representa la solicitud de prestamo creada.
        status code:\n
        - 201: Solicitud creada con exito
        - 400: Faltan campos obligatorios
        - 401: No autorizado
    '''
    
    queryset = LoanApplication.objects.all()
    serializer_class = LoanApplicationSerializer
    permission_classes = [IsPostOrIsAuthenticated]
    
    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
    
    def post(self, request, *args, **kwargs):
        request.data['resultado_aprobacion'] = get_aprobacion(request.data['dni'])
        return self.create(request, *args, **kwargs)
    

class LoanApplicationDetail(generics.RetrieveUpdateDestroyAPIView):
    '''
    Vista que maneja las solicitudes de prestamo detallada.
    
    Esta vista permite editar, borrar (o buscar) una solicitud de prestamo especifica
    
    Methods:
        PUT, DELETE
    
    Args:
        PUT:\n
        pk: La clave primaria que es el dni\n
        Se puede pasar cualquiera los siguientes argumentos:\n
        {
            'dni': dni,
            'nombre': nombre,
            'apellido': apellido,
            'genero': genero,
            'email': email,
            'monto_solicitado': monto solicitado
            'resultado_aprobacion': resultado_aprobacion
        }
        
        DELETE:\n
        pk: La clave primaria que es el dni
        
        
    Response:
        PUT: \n
        Retorna un objeto JSON que representa la solicitud de prestamo editada.\n
        status code:\n
        - 200: Solicitud de prestamo editada con exito
        - 401: No autorizado
        
        DELETE: \n
        Retorna un objeto JSON
        status code:\n
        - 204: Solcitud de prestamo eliminada con exito
        - 401: No autorizado
    '''
    queryset = LoanApplication.objects.all()
    serializer_class = LoanApplicationSerializer
    permission_classes = [IsPostOrIsAuthenticated]