import requests
from .constants import URL_MONI, CREDENTIAL

def get_aprobacion(dni):
    url = f"{URL_MONI}/{dni}"
    headers = {'credential': CREDENTIAL}
    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        json_response = response.json()
        return json_response["status"] == "approve"
    else:
        return f'Falló la solicitud a la url {response.status_code}'